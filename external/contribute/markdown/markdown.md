---
layout: page
permalink: /external/contribute/markdown/
shortcut: contribute:markdown
redirect_from:
  - /cards/contribute:markdown
  - /external/cards/contribute:markdown
  - /contribute/markdown
  - /external/external/contribute/markdown/
---
# Markdown

Markdown is a lightweight markup language with plain text formatting syntax which became very popular in past decade and which nowadays serves as a standard in various digital communication channels.

## Main features of Markdown

* It has very simple syntax easy to learn.
* It is already widely used in scientific community.
* It separates content from visual presentation so it allows you to focus on writing, not formatting.
* It is a non-proprietary file format.
* It is powerful - allows to format text with very little effort.
* It is portable - since it is actually plain text, it can be opened by literally all text editors.
* It is machine readable - as simple text, markdown documents can be tracked and version using a versioning system (Git, SVN)
* It has small file size.
* It is easy to convert to other formats - existing editors and command line tools (e.g. [pandoc](https://pandoc.org/) allows for easy conversion between Markdown and other widely used formats like HTML, PDF, docx, LaTeX, etc.

## Quick reference

* The following symbol <img src="img/visual-code_img_9.png" height="20"> behind the file name means that your changes/writing is not saved. Press CTRL+S to save your procedure
* To preview your writing click on:
  <img src="img/visual-code_img_10.png">

## Tips to write in markdown:

| Markdown                           | Rendered Output           |
|:-----------------------------------|---------------------------|
|: <pre># Header 1</pre>              | # Header 1
|: <pre>## Header 2</pre>             | ## Header 2
|: <pre>### Header 3</pre>            | ### Header 3
|: <pre>#### Header 4</pre>           | #### Header 4
|:-----------------------------------|---------------------------|
|: <pre>To make a text **bold**: use **double asterisk** or __underscores__</pre> | To make a text **bold**, use **double asterisk** or __underscores__ before and after a word or phrase. |
|:-----------------------------------|---------------------------|
|: <pre>To make a text *italic*: use *one asterisk* or _underscore_.</pre> | To make a text **italic**, use *one asterisk* or _underscore_.|
|:-----------------------------------|---------------------------|
|: Use numbers for ordered lists     |
|: ^^<pre>1. First item              | ^^ 1. First item
|: ^^1. Second item                  | ^^ 1. Second item
|: ^^1. Third item                   | ^^ 1. Third item
|: ^^     1. Indented item           | ^^     1. Indented item
|: ^^     1. Indented item           | ^^     1. Indented item
|: ^^1. Fourth item </pre>           | ^^1. Fourth item
|:-----------------------------------|---------------------------|
|: For unordered lists use asterisk, minus or plus |
|: ^^<pre>- First item               | ^^ - First item
|: ^^- Second item                   | ^^ - Second item
|: ^^* Third item                    | ^^ * Third item
|: ^^     * Indented item            | ^^      * Indented item
|: ^^     + Indented item            | ^^      + Indented item
|: ^^* Fourth item</pre>             | ^^ * Fourth item
|:-----------------------------------|---------------------------|
|: Include links referring to a web `[page](https://www.markdownguide.org/)`. Avoid links in format `<link>` as it is not always parsed correctly. | Include links referring to a web [page](https://www.markdownguide.org/). Avoid links in format <link> as it is not always parsed correctly (like in this case).
|:-----------------------------------|---------------------------|
|: Include local pictures using markdown |
|: ^^<pre>![My awesome picture](img/r3_logo.png)</pre> | ^^ ![My awesome picture](img/r3_logo.png)
|: Or use HTML tag allowing you to alter the image properties (e.g. size) |
|: ^^<pre>&lt; img src="img/r3_logo.png" width="40" &gt;</pre> | ^^<img src="img/r3_logo.png" width="40">
|:-----------------------------------|---------------------------|
|: Include code blocks!              |
|: ^^ <pre>\`\`\`                    | ^^
|: ^^def function(x):                | ^^<pre>def function(x):
|: ^^  x+1                           | ^^    x+1
|: ^^function(2)                     | ^^function(2)
|: ^^\`\`\`</pre>                    | ^^</pre>
|:-----------------------------------|---------------------------|

For creating tables, follow the [Jekyll Spaceship documentation](https://github.com/jeffreytse/jekyll-spaceship).
This is an overview of basic Markdown features. For more in-build features, please see various online [documentation](https://www.markdownguide.org/basic-syntax/).

### Advanced formatting

Plain syntax is usually enough. But special requirements lead to development of many of so called ["flavored" markdown syntaxes](https://github.com/commonmark/commonmark-spec/wiki/markdown-flavors) implementing more formatting functionality such as linking different sections, highlighting blocks of code, table extensions, strikethroughs, ...

Another big advantage of Markdown is that it can contain HTML tags, which makes formatting very flexible.

## When to use markdown?

* **Documentation** - Markdown is a perfect solution for description of a data package, project folder, workflow or code repository. Using Markdown ensures that the description will be accessible to everyone even after decades while still nicely structured. Guide for writing a good README is not covered by this HowTo page but you can find plenty of resources online, e.g.:
  * [guide](https://data.research.cornell.edu/content/readme) from Cornell University, UK
  * [GitHub page](https://github.com/mhucka/readmine) for READMEs in a software repository
* **Blogging and tutorials** - structured document with chunks of code, pictures and results of the analyses can be easily converted to HTML format and posted on personal/team websites.
* **Notes and meeting minutes** - you can use following template for simple and nicely structured meeting notes:


| Markdown                           | Rendered Output           |
|:-----------------------------------|---------------------------|
|: <pre># DocTitle | # DocTitle
|: ^^### Attendees: |: ^^ <h3>Attendees:</h3>
|: ^^### Location: |: ^^ <h3>Location:</h3>
|: ^^## Objective / Agenda |: ^^ <h2>Objective / Agenda</h2>
|: ^^## Meeting Notes |: ^^ <h2>Meeting Notes</h2>
|: ^^  * first note |: ^^   * first note
|: ^^  * second note |: ^^   * second note
|: ^^  * ... |: ^^   * ...
|: ^^## Actions |: ^^ <h2>Actions</h2>
|: ^^- [ ] Josh, Alice and Cindy will do this. |: ^^ - [ ] Josh, Alice and Cindy will do this.
|: ^^- [x] Bob stops doing that. |: ^^ - [x] Bob stops doing that.
|: ^^- [ ] Will and Tom will take care of those </pre> |: ^^ - [ ] Will and Tom will take care of those

* **Analyses, reports and papers** - embedding [R markdown](https://rmarkdown.rstudio.com/) in an R project allows you to include executable code and related text in the very same document. This brings unprecedented level of consistency, shareability and reproducibility into the workflows.
* **Online discussion forums** - many scientific and non-scientific discussion forums and social networks support Markdown for formatting the content of messages (e.g. [StackOverflow](https://stackoverflow.com/editing-help), [GitHub](https://help.github.com/en/github/writing-on-github/basic-writing-and-formatting-syntax), Reddit or even Facebook)
* **Presentations** - variety of tools support Markdown-powered slide generation, e.g. [reveal.js](https://revealjs.com/#/), [HackerSlides](https://github.com/jacksingleton/hacker-slides) or [landslide](https://github.com/adamzap/landslide)

## Cases when Markdown is not easily applicable

* **Official documents** like legal agreements and project proposals could be in theory written in Markdown but it is not yet accepted by administrative processes.
* **Big structured tables** with multilevel headers or special formatting can be written in HTML editors and then pasted into your Markdown document.
* **Thesis writing** requires support of citation managers and additional formatting. This can be achieved but usually is conditioned by embedding Latex into your workflow.

## Editors
Markdown file can be written in any text editor. There are many editors supporting rendering and live preview, here are some of them:

* Freeware
  * Atom
  * Dillinger
  * Visual Studio Code
  * ghostwriter
  * Typora
  * HackMD
  * ...
* Payware:
  * iA Writer
  * SublimeText
  * ByWord
* Online
  * [GitHub pages](https://pages.github.com/)
  * [stackedit.io](https://stackedit.io)
