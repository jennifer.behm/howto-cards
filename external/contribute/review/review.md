---
layout: page
permalink: /external/contribute/review/
shortcut: contribute:review
redirect_from:
  - /cards/contribute:review
  - /external/cards/contribute:review
  - /contribute/review
  - /external/external/contribute/review/
---

# Reviewing in Git

Before a branch can be merged to the *develop* branch, a review is often requested. If you have been desginated as the reviewer of a merge request, it means that your approval is needed to be able to proceed with the merge of the branch.

To do the review, click on the link sent by GitLab. You will be redirected to GitLab and directly on the merge request.

You can see the name of the merge request (*LCSB-SOP-ADM-001*), and the history of the merge request in *Overview*. 

- Click on **Changes**

<div align="center">
<img src="img/img1.png">
</div>

- On the left side is the list of the files impacted by this merge request. 

A red square next to a file means that the file has been removed.

An orange one means that the file has been modified.

A green one means that the file has been added.

<div align="center">
<img src="img/img2.png">
</div>

- Click on the **.md file**

    - *Red sections* have been deleted or modified
    - *Green sections* are the modified version of the red sections or are newly added

    <div align="center">
    <img src="img/img3.png">
    </div>

    - If there was no change compared to the previously approved the section will not be highlited. If several lines have not been impacted, they might be collapsed. To uncollapse the lines, click on the arrows.

    <div align="center">
    <img src="img/img4.png">
    </div>

To have a more readable view, with the pictures integrated, click on the 3 dots on the right side and click on *View files @ xxxxx*.

<div align="center">
<img src="img/img5.png">
</div>

- To make a comment on a line, place your mouse on the line number and click on the bubble that appears on the left side. 

<div align="center">
<img src="img/img6.png">
</div>

- Make your comment on the comment section and click on **Start a review** or on **Add comment now**. Clicking on *"Start a review"* will allow you to compile several comments before an email is sent to the assignee when you will click on **Finish review**. Clicking on *"Add comment now"* will send an email to the assignee for every comment.

<div align="center">
<img src="img/img7.png">
</div>

<div align="center">
<img src="img/img8.png">
</div>

When all the threads are resolved, you will be able to **Approve** the merge request and the repository maintenair can proceed with the merge.
