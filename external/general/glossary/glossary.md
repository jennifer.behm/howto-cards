---
card_order: 500
layout: page
permalink: /external/general/glossary/
shortcut: general:glossary
redirect_from:
  - /cards/general:glossary
  - /external/cards/general:glossary
  - /general/glossary
  - /external/external/general/glossary/
---
# Glossary

Are you sometimes confused with all the abbreviations and names in and around the LCSB? Here you find an overview of the most common names abbreviation you may come across during your time at the LCSB.


|Short|Full name|What is it?|
|------|--------|---------|
|AsperaWeb|-|Software for end-to-end encrypted data transfer at the LCSB|
|Atlas|-|Server of the LCSB|
|BT1 & BT2|House of Biomedicine/Biotech 1&2|Indication of the different buildings of the LCSB|
|CFL|Chemins de Fer Luxembourgeois|Operator for public transport (train, bus) in Luxembourg|
|CHL|Centre Hospitalier de Luxembourg|Central Hospital in Luxembourg city – also collaborating with the LCSB|
|CNER|Comité National d’Ethique de Recherche|National committee for ethical approval or research projects|
|CNS|Caissee Nationale de Santé|National health insurance company in Luxembourg|
|DAISY|DAta Information SYstem  |Comprehensive tool to manage your research data in a GDPR-compliant manner. For access, [login with you UNI.LU-account at DAISY](https://daisy.lcsb.uni.lu).|
|DLSM|Department of Life Science and Medicine|Department of the FSTM, formerly called LSRU|
|DTU|Doctoral Training Unit|Umbrella for organization of PhD students from different groups working on a common project|
|Docebo|-|Internal learning platform of the LCSB. [Visit Docebo](https://unisupport.docebosaas.com/learn/signin)|
|ESS|Employee Self-Service|Platform to change your personal data and view your employee profile. Accessible via the intranet |
|Fiori|-|View your payslips and request holidays. Only accessible when logged into the internal network: [click here](https://fiori.uni.lu/fiori/)|
|FNR|Fonds National de la Recherche|Main funder of research activities in Luxembourg investing both public funds and private donations|
|FSTM|Faculty of Science, Technology and Medicine|One of the three faculties at the University of Luxembourg|
|GitLab|-|Web-based application for implementation and management of software |
|KTT/KT4|Building where Ketterthill is located|Indication of the different buildings of the LCSB|
|LIH|Luxembourg Institute of Health|Public biomedical research organisation. Works closely together with the LCSB|
|LIMS|Laboratory Information Management System|Software to manage samples and associated data|
|LIST|Luxembourg Institute of Science and Technology|Research Institute at Belval Campus developing market-oriented services and prototypes|
|LSRU|Life Science Research Unit|Former name for a department of the FSTM. Now called DLSM|
|LUMS|LCSB User Management System|Provides you access to most LCSB internal software. **Requires an additional account besides your UNI.LU-login!**|
|MSA|Maison du Savoir|Central building of the University of Luxembourg also hosting administration|
|OWA|Outlook Web Access|To check your emails from anywhere via a web-browser. Login with you UNI.LU-credentials at [owa.uni.lu](https://owa.uni.lu)|
|R3|Responsible and Reproducible Research|Initiative of the Bioinformatics Core group to raise research quality|
|SAP|Systeme, Anwendungen und Produkte|Software for data management and information exchange|
|SIL|Service of Infrastructure and Logistics|Support for workplace and transportation. You can contact them via the online ticketing system at [service.uni.lu](https://service.uni.lu)|
|SIU|Service Informatique de l’Université|Unit that organizes, administrates and develops IT platforms & systems at the University of Luxembourg|
|Slack|-|Online-based instant messaging platform to organize and work on projects together|
|SOP|Standard Operation Procedure|Internal guideline how common tasks should be executed|
|VM|Virtual Machine|Entire operating system running inside another operating system.|
|VPN|Virtual Private Network|Allows you to connect to the internal university pages, services and servers via a program (client) upon login with your university credentials|





