---
card_order: 300
layout: page
permalink: /external/on-offboarding/onboarding/
shortcut: on-offboarding:onboarding
redirect_from:
  - /cards/on-offboarding:onboarding
  - /external/cards/on-offboarding:onboarding
  - /on-offboarding/onboarding
  - /external/external/on-offboarding/onboarding/
  - /external/on-offboarding/godparent
  - /external/on-offboarding/checklistGodparent
  - /external/on-offboarding/checklistGetStarted
legacy_from:
  - /external/on-offboarding/godparent
  - /external/on-offboarding/checklistGodparent
  - /external/on-offboarding/checklistGetStarted
---

# Onboarding new members at the LCSB

## Purpose

The LCSB regularly welcomes newcomers who have just joined one of the research groups or the support teams. The on-boarding process aims to ensure their smooth integration within their group and within the LCSB. It completes the introduction at the Welcome Day of the university by providing information specific to the LCSB. On top of practical information, this process should convey the LCSB values and the way we all work together.


## 1. To do as a team assistant

The [team assisstants](https://howto.lcsb.uni.lu/?general:secretariat) support the scientists to navigate through the administrative environment at the LCSB. To make sure the newcomer feels comfortable in the new group, the team assistant takes care of the following procedures:

- Align with PI to find a godparent.
- Align with PI which PC is foreseen for the newcomer: either using an old spare one or ordering a new one.
  * Spare PC: request a reinstallation from the IT department ([open a ticket](https://service.uni.lu/sp?id=sc_cat_item&sys_id=ead9a1ecdbb358102fa838aa7c9619df&sysparm_category=c41b4681db9f10502fa838aa7c961990)).
  * New PC: budget code (OTP) needs to be specified, [request an offer from IT](https://service.uni.lu/sp?id=sc_cat_item&sys_id=5ea27df9dbbb50508bcbf9b41d96195f&sysparm_category=c41b4681db9f10502fa838aa7c961990): specify type of keyboard (Qwertz, Qwerty UK/US, Azerty) and accessories like mouse, adapters, etc., create PO and place the order. Keep in mind that this process needs up to 8 weeks.
- Align with PI to define a desk for the newcomer and update [Archibus](https://archibus.uni.lux/archibus/) accordingly.
- Request a badge and the necessary office access in [Archibus](https://archibus.uni.lux/archibus/) (lab access is granted by the lab support team accordingly).
- Open tickets to
  * Order a [phone and a phone extension](https://service.uni.lu/sp?id=sc_cat_item&sys_id=9f11411adbd69c902fa838aa7c961959&sysparm_category=1679cecddb5f10502fa838aa7c9619ab) attributed to the newcomer
  * Add newcomer to [mail lists](https://service.uni.lu/sp?id=sc_cat_item&sys_id=b7b07412db969c902fa838aa7c9619ca&sysparm_category=0784418e1b312450ff1c8739cd4bcb44) (e.g., LCSB-team, group list).
  * Get access to the [intranet](https://service.uni.lu/sp?id=sc_cat_item&sys_id=448026d8dbd07450c542f9b2f39619c6&sysparm_category=0784418e1b312450ff1c8739cd4bcb44&catalog_id=26a236a5dbd2d8902fa838aa7c961948), [shared folders](https://service.uni.lu/sp?id=sc_cat_item&sys_id=d8c9740e1bfde050ff1c8739cd4bcb94) on the ATLAS server, LUMS [account](https://service.uni.lu/sp?id=sc_cat_item&sys_id=c536257ddb336010ca53454039961936&sysparm_category=6b76697ddb336010ca534540399619f8), etc.
- Prepare a welcome bag with office supplies pens, notebook, scissors, stapler, ruler etc. and give it to the newcomer on the first day.
- Meet with newcomer to explain the different administrative procedures and existing tools such as travel organization, TAR, PO, expense reports, the Service Now ticket system, HR.
- Order business cards via [Web-to-Print](https://www.webtoprint.lu/uni/UserEditFormFilling.aspx) if needed.

## 2. To do as a godparent

When a new member joins the LCSB, the group PI is asked to nominate a senior member of the group as “godparent” who guides the newcomer during the on-boarding process. The godparent is showing the newcomer around the LCSB premises (BT1, BT2, Ketterthill, Roudeneck/BTL and MNO) and introduce the newcomer to the team and to the relevant LCSB staff (e.g., communication (BT2), financial (Ketterthill) and grants (MNO) teams, support biotech and the team assistant, and takes care of some of the early procedures:

- Open tickets to
  * Pick up the computer on the first day of the newcomer
  * Pick up the entrance badge together with the newcomer at the Security office in MSA 1st floor
- Show how to get office supplies (communication office in BT2).
- Explain Uni/LCSB [intranet](https://uniluxembourg.sharepoint.com/sites/the_university), newsletter, services and [important links](https://howto.lcsb.uni.lu/?general:links).
- Explain the lunch voucher system and where to get food.
- Explain the regular meetings: team meeting (mandatory), group meetings, TGIaF, CETs for uni.lu PhD candidates, etc
- Make sure the newcomer follows Biosafety trainings (Biotech Support Team).
- Make sure the newcomer follows the “Data management and data protection” training ([LCSB data stewards](mailto:lcsb-datastewards@uni.lu)).
- Encourage the newcomer to participate in the welcome meetings of UL and LCSB (see 4.).
- Encourage the newcomer takes an appointment for the Uni photo shoot (Newcomer should get an email).

## 3. To do as a newcomer

- Get acquainted with your colleagues, especially your group, the group secretary, and research support teams (ask your godparent to introduce you).
- Familiarise yourself with different LCSB tools e.g., ATLAS server, LUMS account, owncloud, the regular meetings (LCSB team meeting (mandatory), TGIaF, group meetings, CETs if you are a uni.lu PhD candidate), the lunch vouchers distribution, etc.
- Uni/LCSB policies and culture: how to find details about them on the [intranet](https://uniluxembourg.sharepoint.com/) and [How-to Cards](https://howto.lcsb.uni.lu/), and [important links](https://howto.lcsb.uni.lu/?general:links)
- Read the bimonthly LCSB newsletter and explore the intranet to stay up to date with what is happening at the LCSB. Send an email to [ruxandra.soare@uni.lu](mailto:ruxandra.soare@uni.lu) if you don’t receive the newsletter.
- Fill in your profile on [LCSB intranet](https://uniluxembourg.sharepoint.com/sites/lcsb/lscb_internal). Check that your information on the personal webpage on the University website is accurate and that you are mentioned in LCSB people list and in the group member list on the website.
- Set up the proper email signature and confidentiality disclaimer for your university mailbox: [Guidelines in the How-to Cards](https://howto.lcsb.uni.lu/?integrity:LCSBEmailSignature)
- Complete your trainings to keep access to the lab and get further access to the labs.
- Complete your trainings on data management and data protection.
- Take an appointment for the Uni photo shoot (you should get an email).

## 4. Welcome meetings

- **University Welcome Day**: The newcomers must attend the University Welcome Day to get a general overview of the university procedures and policies. You should get an invitation from HR Administration. The presenter will explain you, where to store and find personal & general information and to understand globally the UL's tools for their daily requests: Atlas Server, Ticket system, HR4U, Fiori platform, Intranet, Archibus and more. In addition, you will get general information on the medical examination, social security, child allowances and renumeration.
- **LCSB Welcome meeting**: Once a month a two hours meeting is organised at the LCSB for the members who arrived recently. During the first part, a round table allow participants to introduce themselves and to place their “location sticker” on the LCSB map in BT2 staircase. Then, a member of the communication team presents the LCSB, its goals and values, and provides some practical information (who’s who, basic procedures, where to find answers, etc.). The second part is dedicated to security and safety (building evacuation, emergency exits, lab safety…).
The participants also get a copy of the LCSB annual and research reports and the slides used during the meeting are shared. The newcomers receive a calendar invitation for this meeting, sent by the communication team.

In order to introduce the newcomers to the whole LCSB team, the new members are mentioned in the LCSB newsletter (Photo, name, position and email address in the “Newcomers” section). They can also introduce themselves during the weekly team meeting at the LCSB.

## 5. Services and websites

You will need two accounts to make use of services and tools:
* **UNI-LU account:** To use the IT services, also known as SIU, will give you your account at your first working day
* **LUMS account:** This account is used for particular services (see below) and will be created after you have your UNI-LU account. Your godparent can assist you in that.

#### The **UNI-LU account** is used to access the following services:

* Mail services, including web mail accessible from the browser via: [https://owa.uni.lu](https://owa.uni.lu)
* Microsoft Office & Teams: [https://portal.office.com](https://portal.office.com)
* University’s Service Portal to create tickets for IT, office setup, and other issues: [https://service.uni.lu](https://service.uni.lu)
* SAP, to make leave requests, see and print payslips and other HR-related services: [https://fiori.uni.lu/fiori](https://fiori.uni.lu/fiori)
* Webex which is used for all virtual work meetings (implemented due to the COVID-19 pandemic), unless the other party requires the use of other software. You can login with UNI-LU account at: [https://unilu.webex.com/](https://unilu.webex.com/)
* University Intranet: [https://uniluxembourg.sharepoint.com/](https://uniluxembourg.sharepoint.com/)
* LCSB Intranet: [uniluxembourg.sharepoint.com/sites/lcsb/lscb_internal](https://uniluxembourg.sharepoint.com/sites/lcsb/lscb_internal/SitePages/default.aspx)
* specific communication channels, such as Slack, Basecamp an others, depending on your team. Your godparent will provide you with further information.


#### The LUMS account is used to access the following services:

* Owncloud, a file cloud deployment at LCSB: [http://owncloud.lcsb.uni.lu](http://owncloud.lcsb.uni.lu)
* Gitlab, where the bioinformatics core team we manages its work products, source code and websites: [https://gitlab.lcsb.uni.lu/](https://gitlab.lcsb.uni.lu/)
* other specific and licence-dependent software that is relevant for your work

#### Enjoy your time at the LCSB!
If you have any questions, [do not hesitate to ask](https://howto.lcsb.uni.lu/?handbook:2-contact)!
