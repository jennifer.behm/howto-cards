---
layout: page
permalink: /external/lab-equipment/lightcycler/
shortcut: lab:lightcycler
redirect_from:
  - /cards/lab-equipment:lightcycler
  - /external/cards/lab-equipment:lightcycler
  - /lab/lightcycler
  - /external/external/lab-equipment/lightcycler/
  - /cards/lab:lightcycler
  - /external/cards/lab:lightcycler
---

# How to leave virtual instrument mode on the LightCycler

### Error message

1.	Here are a few of the error messages you can encounter when the instrument is in Virtual mode:

- When opening the software, you might see the message: ***“Please activate an instrument before setting up a new run”***


- When loading a run template, you might see: ***“Template’s filter combinations do not match. Template is for a different sized instrument block”*** (this will also appear if you try to load a program designed for 96 well plates on the 384-well plate block and vice versa)

<div align="center">
<img src="img/img2.png" width="600">
</div>

2.	To properly identify the issue and make sure you are in Virtual mode, simply check the ***“Instrument”*** line of the software. If you are in virtual mode, it will be the first word of the instrument name. If you are not in virtual mode, the name might be ***“New Instrument”*** or ***“LC480”***

- Virtual: 

<div align="center">
<img src="img/img3.png" width="600">
</div>

- Normal mode:

    - ***“Standby (no MWP)”*** means that the instrument is on, but no plate has been loaded

    - ***“Not connected”*** means that the instrument is off.
    

<div align="center">
<img src="img/img4.png" width="600">
</div>
<div align="center">
<img src="img/img5.png" width="600">
</div>


## Fix the issue

In order to leave the virtual mode please follow the steps:

- On the right-side panel, go to ***“Parameters”***

<div align="center">
<img src="img/img6.png" width="600">
</div>

- Click on ***“Instruments”***

<div align="center">
<img src="img/img7.png" width="600">
</div>

- In virtual mode, the name of the instrument will start with ***“Virtual”*** and the IP address will only contain 0.

<div align="center">
<img src="img/img8.png" width="600">
</div>

- In the ***“Instruments”*** drop down menu, select the instrument called ***“New Instrument”*** or ***“LC 480”*** (depending on the databases). This instrument will have a valid IP address

- Click on ***“Make Default”***

<div align="center">
<img src="img/img9.png" width="600">
</div>

- Click on ***“Close”***

<div align="center">
<img src="img/img10.png" width="600">
</div>

**You should now be able to use the instrument.** 
