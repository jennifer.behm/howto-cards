---
layout: page
permalink: /external/lab-equipment/maintenance-of-fridges/
shortcut: lab:maintenance-of-fridges
redirect_from:
  - /cards/lab-equipment:maintenance-of-fridges
  - /external/cards/lab-equipment:maintenance-of-fridges
  - /lab/maintenance-of-fridges
  - /external/external/lab-equipment/maintenance-of-fridges/
  - /cards/lab:maintenance-of-fridges
  - /external/cards/lab:maintenance-of-fridges
---
# Maintenance of fridges and freezers

Routine maintenance of fridges and freezers prevents from issues and preserves integrity of samples.

The fridges and freezers are under constant temperature monitoring. In order to avoid alarms, the monitoring has to be disconnected before the cleaning of 4°C fridges and the defrosting of -20°C/-80°C freezers (see [How to disable a Rmoni sensor](#how-to-disable-a-rmoni-sensor)). 

**Please inform the Instrument Care team before every kind of maintenance by sending [a ticket](https://service.uni.lu/sp?id=sc_cat_item&sys_id=6ea39fdbdbec38105c72ef3c0b96196a&sysparm_category=af924f17dbac38105c72ef3c0b96194b) 24h before starting your maintenance**. Preferentially start your maintenance in the morning. When the maintenance is finished and the temperature is back to normal, reply on the open ticket to let them know you are done.

All users are in charge of the maintenance of the fridges and freezers at the following frequency:

<div align="center">
<img src="img/Tab1.png" width="600">
</div>

## How to deactivate the alarm in Sensor4Lab

All the freezers are monitored via a software called Sensor4Lab. To avoid any alarm due to the defrosting, the first step of the defrosting process is always to put the monitoring on hold. This step is needed for the cleaning of 4°C fridges and defrosting of -20°C/-80°C freezers.

**Only technicians have access to Sensor4lab**. Please ask a technician of your team to disable the sensor associated to the fridge you want to clean or defrost. 
-	If you’re a technician, go on [Sensor4lab](https://sensor4lab.lcsb.uni.lu/account/logon) and login with your credentials received from the Instrument Care Team. If you don’t have any credentials yet, please send [a ticket](https://service.uni.lu/sp?id=sc_cat_item&sys_id=dc205fd7dbec38105c72ef3c0b96191d&sysparm_category=af924f17dbac38105c72ef3c0b96194b) 
-	Please follow the process explained [here](https://dutycall.lcsb.uni.lu/dutycall/deactivate-alarm)

Please note that there is a 30 minutes delay between the moment that you disable the sensor and the response of the system. This means that you should disable the monitoring 30 min before you start the maintenance.

## How to clean a fridge
### 1. Bioline: BioPlus – BioMidi
This section applies to the fridges from GRAM, models BioPlus and BioMidi.
<div align="center">
<img src="img/img5.png" width="300">
</div>

-	**Once per quarter**, the door and gasket must be inspected and cleaned with a mild soap. Make sure the door and the gasket are dry before closing back. Fill the maintenance form located on the door of the fridge.
-	**Once a year**, the cabinet should be cleaned as follow:
    -	24h before your maintenance, inform the Instrument Care team by sending [a ticket](https://service.uni.lu/sp?id=sc_cat_item&sys_id=6ea39fdbdbec38105c72ef3c0b96196a&sysparm_category=af924f17dbac38105c72ef3c0b96194b) and [disable the Sensor4Lab sensor](#how-to-disable-a-rmoni-sensor)
    -	Switch off the fridge and pull out the main plug
    -	Transfer all content in another fridge and clean the inside of the fridge using a mild soap solution or 70% ethanol
    -	Make sure the inside of the fridge is dry
    -	Reconnect the main plug and switch the fridge back on
    -	Wait until the pre-set temperature is reached before transferring the content of the fridge back
    -	Enable the Rmoni sensor and inform the Instrument Care team via ticket that you are done and that everything is back to normal
    -	Fill the maintenance form located on the door of the fridge



### 2. Liebherr LKEXv / Liebherr LKPv
This section applies to the LIEBHERR fridges (141 and 360 L and 600L) and to the fridge part of the combined fridge / freezers from LIEBHERR LC.
<div align="center">
<img src="img/img6a.png" width="300">
</div>

-	The appliance defrosts automatically in Liebherr LKEXv fridges. The defrost water drains into a tray located below the evaporator. **Once per quarter**, the defrost water tray should be emptied and cleaned. Fill the maintenance form located on the door of the fridge.
<div align="center">
<img src="img/img7.png" width="150">
</div>

-	**Once a year**, the cabinet should be cleaned as follow:
    -	24h before your maintenance, inform the Instrument Care team by sending [a ticket](https://service.uni.lu/sp?id=sc_cat_item&sys_id=6ea39fdbdbec38105c72ef3c0b96196a&sysparm_category=af924f17dbac38105c72ef3c0b96194b) and [disable the Rmoni sensor](#how-to-disable-a-rmoni-sensor)
    -   Switch off the fridge and pull out the main plug
    -	Transfer the content of the fridge to another fridge and clean the inside with lukewarm water and mild detergent. **Ensure that no cleaning water penetrates into the electrical components of ventilation grids**
    -	Dry the inside of the fridge
    -	Reconnect the main plug and switch the fridge back on
    -	Wait until the pre-set temperature is reached before transferring back the content of the fridge
    -	Enable the Rmoni sensor and inform the Instrument Care team via ticket that you are done and that everything is back to normal
    -	Fill the maintenance form located on the door of the fridge

<div align="center">
<img src="img/img6b.png" width="240">
</div>


-	The freezers LIEBHERR LGPv (600 L) defrost automatically, and therefore, the user does not need to do it.
-	**Once a year**, the cabinet should be cleaned as follow:
    -	24h before your maintenance, inform the Instrument Care team by sending [a ticket](https://service.uni.lu/sp?id=sc_cat_item&sys_id=6ea39fdbdbec38105c72ef3c0b96196a&sysparm_category=af924f17dbac38105c72ef3c0b96194b) and [disable the Rmoni sensor](#how-to-disable-a-rmoni-sensor)
    -   Switch off the freezer and pull out the main plug
    -	Transfer the content of the freezer to another freezer and clean the inside with lukewarm water and mild detergent. **Ensure that no cleaning water penetrates into the electrical components of ventilation grids**
    -	Dry the inside of the freezer
    -	Reconnect the main plug and switch the freezer back on
    -	Wait until the pre-set temperature is reached before transferring back the content of the freezer
    -	Enable the Rmoni sensor and inform the Instrument Care team via ticket that you are done and that everything is back to normal
    -	Fill the maintenance form located on the door of the fridge

## How to defrost a freezer
After some time and depending of its opening frequency, ice can be formed inside of the freezers. To ensure a good maintenance, a reliable temperature and to keep the energy consumption as low as possible, this ice has to be removed on a regular basis. It is on the responsibility of the lab users to remove the excess of ice. 
-	[How to defrost a -20°C freezer](#how-to-defrost-a--20c-freezer)
-   [How to defrost a -80°C freezer](#how-to-defrost-a--80c-freezer)
-	[How to defrost a -150°C freezer](#how-to-defrost-a--150c-freezer)

### How to defrost a -20C freezer
At LCSB, we have different kind of -20°C freezers. The defrosting procedure is the same for all of them, except for the Liebherr LGPv. Those freezers defrost automatically, and therefore, the user does not need to do it. 
<div align="center">

LIEBHERR LGPv (600 L): **no defrosting needed**
</div>

<div align="center">
<img src="img/img8.png" width="300">
</div>

For the other freezers (Liebherr LGUex and Liebherr LC), the defrosting should be performed:

	**Every three months** for freezers that are often opened and closed

	**Every six months** for the other freezers
<div align="center">
<img src="img/img9.png" width="600">
</div>

-	24h before your maintenance, inform the Instrument Care team by sending [a ticket](https://service.uni.lu/sp?id=sc_cat_item&sys_id=6ea39fdbdbec38105c72ef3c0b96196a&sysparm_category=af924f17dbac38105c72ef3c0b96194b) and [disable the Rmoni sensor](#how-to-disable-a-rmoni-sensor)
-	Switch off the appliance and pull out the main plug
-	Remove the drawers and transfer their content to another freezer
-	To speed up the defrosting process, a beaker containing warm, but not boiling, water can be put on one of the cooling plates
-	Leave the door open for defrosting
-	Check regularly and mop the melted water
-	Once all the ice melted and the water has been mopped, clean the inside of the freezer with lukewarm water and mild detergent. **Ensure that no cleaning water penetrates into the electrical components or ventilation grids**
-	Dry completely the inside of the freezer
-	Reconnect the main plug and switch the freezer back on
-	Wait until the pre-set temperature is reached before transferring the content of the freezer back
-	Enable the Rmoni sensor and inform the Instrument Care team that you are finished and that everything is back to normal
-	Fill the maintenance form located on the door of the freezer

### How to defrost a -80C freezer
This section describes the maintenance steps of the Ultra-Low Temperature Freezers from Sanyo (MDF-U76V-PE, MDF-U76V-P and MDF-U74V).

#### **Inner doors defrosting**
With repeated opening and closing, frost builds on the inner doors of the chamber. Excessive frost can cause some gaps between the cabinet and the magnetic gasket, resulting in poor cooling. 
-	**Once a month**, the frost from the inner doors need to be removed
-	Open the outer and inner doors and remove the inner doors by lifting them up
<div align="center">
<img src="img/img10.png" width="150">
</div>

-	Place the inner doors in a dedicated plastic box to let the ice melt
-	Dry the doors well before placing them back in the freezer
-	Use a scraper to defrost the inner parts of the chamber
<div align="center">
<img src="img/img11.png" width="150">
</div>
Scrape the inside where the doors are in contact with the chamber (see arrows on the picture)
<div align="center">
<img src="img/img12.png" width="200">
</div>

-	If the rubber bands around the inner doors are damaged, contact the Instrument care team for replacement by sending [a ticket](https://service.uni.lu/sp?id=sc_cat_item&sys_id=6ea39fdbdbec38105c72ef3c0b96196a&sysparm_category=af924f17dbac38105c72ef3c0b96194b)
-	Fill the maintenance form located on the door of the freezer


#### **Air intake port cleaning**
When the freezer door is closed, the warm air that entered the chamber cools down rapidly, and contracts causing a negative pressure. This is the reason why it is difficult to open the freezer shortly after it was closed.

Some -80°C freezers are equipped with an air intake port, which allows to counteract the negative pressure in the chamber. It is found on the left side of the freezer, by the door handle:
<div align="center">
<img src="img/img13.png" width="180">
</div>
Turn the cap counter clockwise about two laps. Then, let the air enter the chamber for about 20 seconds and open the door. Finally, do not forget to close the cap.

**Once a month**, the air intake port should be cleaned:
-	Take the cap completely out by turning it counter clockwise
-	Using the stick for air intake port cleaning, break the ice inside the port (do not use sharp tools such as knife or screw driver)
<div align="center">
<img src="img/img14.png" width="100">
</div>

-	Make sure to remove all the ice from the cap groove
-	Place the cap back on the air intake port
-	Fill the maintenance form located on the door of the freezer


#### **Condenser filter cleaning**
<div align="center">
<img src="img/img15.png" width="300">
</div>

**Once a month**, the condenser filter should be cleaned:

-	Open the grille by pulling it to you
-	Take out the condenser filter and wash it with water
-	Don’t touch the condenser directly! This may cause injury by hot surface  
-	Replace the condenser filter and the grille
-	Check that the filter check lamp is off
-	Fill the maintenance form located on the door of the freezer 

#### **Inner chamber defrosting**
**Once a year**, the inner chamber should be defrosted:
-   24h before your maintenance, inform the Instrument Care team by sending [a ticket](https://service.uni.lu/sp?id=sc_cat_item&sys_id=6ea39fdbdbec38105c72ef3c0b96196a&sysparm_category=af924f17dbac38105c72ef3c0b96194b) and [disable the Rmoni sensor](#how-to-disable-a-rmoni-sensor)
-	Switch off the freezer (the switch is located at the bottom of the left side of the freezer)
-	Transfer all the boxes to the backup freezer
-	Remove all racks and let them defrost in a dedicated box
-	Remove the inner doors, and let the outer doors open
-	Remove as much ice as you can to speed up the process
-	Once all the ice melted, dry the freezer with a cloth
-	Place back the doors and racks
-	Switch the freezer back on and wait until the pre-set temperature is reached
-	Transfer the samples back
-	Enable the Rmoni sensor and inform the Instrument Care team that you are finished and that everything is back to normal
-	Fill the maintenance form located on the door of the freezer 


### How to defrost a -150C freezer
This section applies to the -150°C freezer (Sanyo MDF-C2156VAN, BT1-407)

<div align="center">
<img src="img/img16.png" width="200">
</div>

#### **Inner doors defrosting**
-	24h before your maintenance, inform the Instrument Care team by sending [a ticket](https://service.uni.lu/sp?id=sc_cat_item&sys_id=6ea39fdbdbec38105c72ef3c0b96196a&sysparm_category=af924f17dbac38105c72ef3c0b96194b) and [disable the Rmoni sensor](#how-to-disable-a-rmoni-sensor)
-	**Once a month**, the frost forming on the inner door can be removed using the scraper
-	Dry the inner door and the edges with a cloth before closing back the door.
-	Inform the Instrument Care team that you are finished and that everything is back to normal
-	Fill the maintenance form located on the door of the freezer

#### **Condenser filter cleaning**
**Once a month**, the condenser filter should be cleaned:
-	Open the grille by pulling it to you
-	Take out the condenser filter and wash it with water
-   Don’t touch the condenser directly! This may cause injury by hot surface  
-	Replace the condenser filter and the grille
-	Check that the filter check lamp is off
-	Fill the maintenance form located on the door of the freezer 
