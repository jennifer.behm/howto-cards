---
layout: page
permalink: /external/lab-equipment/utilization-of-bsc/
shortcut: lab:utilization-of-bsc
redirect_from:
  - /cards/lab-equipment:utilization-of-bsc
  - /external/cards/lab-equipment:utilization-of-bsc
  - /lab/utilization-of-bsc
  - /external/external/lab-equipment/utilization-of-bsc/
  - /cards/lab:utilization-of-bsc
  - /external/cards/lab:utilization-of-bsc
---
# Biosafety Cabinets: good practices

To ensure a good work quality in a sterile and safe environment, it is essential to know how to work in a BSC and what are the best practices.

Please check the following short videos from The World Health Organization (WHO).

1.  [Introduction](https://www.youtube.com/watch?v=KHCT9OJqxPo&t=1) to Biosafety Cabinets

2.  [Preparation Steps](https://www.youtube.com/watch?v=4DoHJS8JL4U) prior to working in a biosafety cabinet (BSC)

3.  [Best Practices](https://www.youtube.com/watch?v=18QEJUA9XBs) for safe biosafety cabinet (BSC) usage

4.  Biosafety cabinet (BSC) [Incident Management](https://www.youtube.com/watch?v=aS_TCZTCcsI)

If you have any issue with a BSC, please send a [ticket](https://service.uni.lu/sp?id=sc_cat_item&sys_id=be0873f8db7070505c72ef3c0b96199f&sysparm_category=af924f17dbac38105c72ef3c0b96194b) to the Support Team.

If you are not sure a BSC is the right hood to use for your experiment, please check the [How To Card](https://howto.lcsb.uni.lu/external/lab-equipment/hoods/) related to the different types of hoods or contact the LCSB safety officer.

## Recommendations

-   The user must wear a BSL2 **lab coat** and **gloves** while using a BSC.

<div align="center">
<img src="img/image1.jpeg">
<img src="img/image2.jpg" width="161" high="190">
</div>

-   Be aware that **UV-lights act only on surfaces exposed to them**, the surfaces in the shadow are not disinfected and a chemical disinfection is still necessary after a UV decontamination.

-   The BSC must be **decontaminated before and after each use**. The interior surfaces should be wiped with an appropriate disinfectant that would kill any microorganisms that could be found in the cabinet. 70% ethanol is not effective against yeast and fungi, you should **use Bacillol 30 foam or tissues**.

<div align="center">
<img src="img/image3.jpeg">
<img src="img/image4.jpeg">
</div>

-   The BSC should be **empty** when you start working. If material is stored inside the BSC (for instance micropipettes), they must be disinfected when you switch on the BSC.

Corrosive chemicals such as bleach should be avoided, but if used, should be followed with a wipe down of sterile water or 70% ethanol. Wipe the work surface, the inside walls, below the work surface and the collecting pan. Do not use non-sterile paper towels.

<div align="center">
<img src="img/image5.png">
</div>

-   Allow the cabinet to run for at least **10 minutes** after switching on.

-   Your **hands** and all the **materials** that enter in the BSC must be **decontaminated with 70% ethanol or Bacillol 30 tissues**.

-   Put **Incidin PLUS** in **the aspiration pump** and disinfect the pump tubing with **Ethanol 70% or Bacillol 30**

<div align="center">
<img src="img/image6.jpeg" width="166" high="267">
</div>

-   Control the cells in the **microscope** before starting to work to **detect contamination** in your cell culture (change in the medium color, cloudiness of the medium, cross-contamination with other cell lines, ...).

-   Set up workspace in a direction **from clean to dirty**.

<div align="center">
<img src="img/image7.png">
</div>

-   **Never cover the front or rear grids** with any material, even temporarily.

-   **Avoid movement of materials or excessive movement of hands and arms** through the front access opening during use. After entering, allow the cabinet to stabilize before resuming work.

-   Take care **not pass** with your hands **over open bottles, flasks or tip boxes**.

-   When you open a flask or a medium, you can place the **lid with the opening up or down** on the bench, but make sure you put it **outside your working area**, at the back of the BSC so you do not pass over it with your hands or arms.

-   **Close open bottles, flask, falcons, tips boxes immediately after use**.

-   **Remove your waste as soon as you have finish your work**.

-   When work is completed, clear the workspace and clean the space with Ethanol 70% or with Bacillol 30. Please refer to the [Chemical and Biological Waste Management](https://howto.lcsb.uni.lu/external/lab-hsa/waste/).


## UV lights

Extract from Environmental Health and Safety, University of Washington: *The trouble with UV light in your biosafety cabinet*.

You may want to turn on the UV lights. If so, be aware that it may not be as effective for sterilization/decontamination purposes as you need it to be.
Indeed, ultraviolet radiation is a form of non-ionizing radiation, and biological effects from it vary with wavelength, photon energy, and duration of exposure. The 100-280 nm wavelength band is designated as UV-C, which is used for germicidal purposes.

The sterilization/decontamination activity of UV lights is limited by a number of factors, including:

- **Penetration** In the dynamic air streams of BSCs, microorganisms beneath dust particles, plastics, and work surfaces are not affected by the UV light because it cannot penetrate particles so far from the UV source.
- **Relative humidity** The germicidal effects of UV light drop off precipitously when relative humidity is above 70%.
- **Temperature and air movement** The optimum temperature for the UV lamp to be effective is 25-27°C. Temperatures below this range result in reduced efficacy, and air movement can exacerbate this. 
- **Cleanliness** Dust and dirt block the germicidal effectiveness of the UV lamp, so weekly cleanings are necessary.
- **Age** Check UV lamps every six months to assure proper function, as the amount of germicidal wavelength emitted decreases with bulb age and hours of use.
- **Overuse** UV lights are routinely left on overnight or longer in an effort to decontaminate workspaces, but this practice can result in the germicidal wavelength no longer being produced by the bulb.
For these reasons and environmental concerns, it is not recommend to use the UV lights in BSCs. Retrofitting any equipment (e.g., UV lights) into a cabinet may alter the air flow characteristics, invalidate the manufacturer warranty, and is not recommended.
