---
card_order: 400
layout: page
permalink: /external/access/vpn-cerbere-access/
shortcut: access:vpn-cerbere-access
redirect_from:
  - /cards/access:vpn-cerbere-access
  - /external/cards/access:vpn-cerbere-access
  - /access/vpn-cerbere-access
  - /external/external/access/vpn-cerbere-access/
---
# VPN/CERBERE connection

In addition to the LCSB account, external collaborators are provided with VPN account so that they can access the servers that are hosted at the LCSB.

## VPN Temporary Password Reset

1. To reset temporary VPN password please visit [PasswordReset](https://passwordreset.uni.lu)

    <img src="img/password-reset-vpn.png" height="300px"><br/>

2. Click on **Change password**

3. Enter

   * Username - firstname.lastname
   * Password - Temporary password provided by Sysadmins for VPN

   <img src="img/password-reset-vpn-01.png" height="300px"><br/>

   * Click **Next**
   * Enter the new password that you want to use.

   <img src="img/password-reset-vpn-02.png" height="300px"><br/>

## Download VPN

Once you have reset your VPN Password, please download VPN client from [VPN](https://vpn.uni.lu/)

**ENTER**

1. Username - firstname.lastname
2. Password - Password you have set for VPN

**Note**- If firstname.lastname does not work, give a try with firstname.lastname@uni.lu
<img src="img/vpn-download.png" height="300px"><br/>

Then depending on your distribution of OS, it will suggest you a link to download.
<img src="img/vpn-download-01.png" height="300px"><br/>

## Connect to Cerbere via VPN

1. Launch the VPN client and enter

   <img src="img/vpn-connect-01.png" height="300px"><br/>

2. Enter

   * Username - firstname.lastname
   * Password - VPN password that you have set.

3. Once you are connected to VPN, download RDP files from [Cerbere](https://cerbere.uni.lu/)

   * Enter same credentials as VPN
     * Username - firstname.lastname
     * Password - VPN password that you have set.

   * Under **My Authorizations**, select **Sessions** and download the file

## Access to the VM

Run the command/script you have downloaded from Cerbere. For example like below -

```ssh -t firstname.lastname@cerbere.uni.lu 'Interactive@als-epilepsy-vm-1:SSH:ALS-epilepsy-Group'```

```
WARNING: Access to this system is restricted to duly authorized users only. Any attempt to access this system without authorization or fraudulently remaining within such system will be prosecuted in accordance with the law.
Any authorized user is hereby informed and acknowledges that his/her actions may be recorded, retained and audited.

user's password: VPN password

Account successfully checked out

Connecting to Interactive@als-epilepsy-vm-1:SSH...
Target login: firstname.lastname
firstname.lastname's password: LUMS Password

You are hereby informed and acknowledge that your actions may be recorded, retained and audited in accordance with your organization security policy.
Please contact your WALLIX Bastion administrator for further information.

Last login: Fri Dec  6 16:43:22 2019 from cerbere.uni.lu
```
