---
card_order: 800
layout: page
permalink: /external/publication/publishInBiotools/
shortcut: publication:publishInBiotools
redirect_from:
  - /cards/publication:publishInBiotools
  - /external/cards/publication:publishInBiotools
  - /publication/publishInBiotools
  - /external/external/publication/publishInBiotools/
---

# Publishing a tool in *bio.tools*

First, *bio.tools* serve two main purposes:

- dissemination of your tools
- collecting systematic and reasonable statistics about the available
  software for many purposes, including data science and performance reports

To get your contents in, you need to create an account (sign up). Once logged-in, you can click "Add content" in the "Menu" on the top
right, then go through around 10 tabs that allow you to fill lots of
interesting information about your tool, and finally submit it, which makes the
contents immediately visible.


## General tips

### Use search and consult the curation guide to resolve ambiguities

The guide is currently [here](https://biotools.readthedocs.io/en/latest/curators_guide.html#summary-group).

It includes helpful and authoritative guidelines for almost all fields that you
can fill, including examples.

As the last resort when you do not know what to do with a field, you can try
search suggestions in the structured search box above, and try to be at least
consistent with the rest of the database. In the search box, type in a single
letter, wait for a moment (or press arrow down), choose the annotation
category, and see what others have used.

### Add a "backup" maintainer

This may remove a lot of unnecessary workload from you, and will help everyone
at times when you are not available. (Or, at least, reduce the stress levels.)

### Add _many_ links to all types of documentation

This helps in two ways:
- The search engines will be able to categorize and index your contents better,
  possibly giving more helpful links to your tools.
- Users of *bio.tools* will be able to explore the most relevant parts of your
  documentation very quickly, helping them to make a faster decision about
  using your tool. For example, it is often a relief to quickly see that a
  bioinformatics tool has an explicit step-by-step tutorial, and that a web
  visualization framework has a complete useful API reference.

Include the (interesting) relations to other tools. This adds a bit more
interesting data to both search engines and statisticians who process
*bio.tools* content, and gives an documentation of possible "use-cases".

## Proper labeling

### Choose the bio.tools ID correctly

As the name says, the "Persistent biotoolsID" cannot be changed. It will be
permanently displayed as an identifier in almost all content that is
autogenerated from *bio.tools*. For users, that concerns mainly the URLs, so
you may want to apply similar formatting guidelines as for URLs (use hyphens
`-` instead of underscores `_`, do not use capital letters, ...).

### Add your tool to proper collections

Collections group the results of various larger cooperated (or loosely
organized) efforts, such as "Galaxy tools", "Parkinson disease research", and
"3D BioInfo".

If your tool is related to ELIXIR, you should fill in the correct ELIXIR node,
platform, community _and_ collection. Remember to:

- Add _all_ involved ELIXIR nodes.
- **Add your tool to the node-specific ELIXIR collections.** For LCSB, that is
  usually **ELIXIR-LU**. This is counter-intuitive and seems duplicate to the
  categorization by ELIXIR nodes, but:
  - at least 2 nodes use collections labels instead of node labels to scrape
    data about their tools
  - there is search support only for the collections; currently (March 2021) it
    seems that you cannot easily search for tools by ELIXIR nodes

On top of that (but also if the tool is not related to ELIXIR) you add further collections:

|Group                        | Collection      |
| --------------------------- | --------------- |
|University of Luxembourg     |            UniLu|
|LCSB                         |             LCSB|
|Biomedical Data Science      |         LCSB-BDS|
|Computational Biology        |         LCSB-CBG|
|Bioinformatics Core          |     LCSB-BioCore|
|Environmental Cheminformatics|         LCSB-ECI|



### Fill in the tool "Function" very verbosely

The main reason for categorizing tool functions is to provide a systematic way
for listing the tools in topic-based catalogues that can be browsed by users.

The categories and data formats available for describing your tool function
rarely give a precise match. That is okay, you can choose a broader category.

Use all categories that match even a small part of your tool; it will
potentially help *bio.tools* users to find your tool even if they do not know
what precisely they are searching for.

Selecting a very specific category ("deeper" in the topic tree) also implies
all "parent" categories, so you do not have to be afraid of over-specification.
*Bio.tools* search box will automatically include your tool in searches for the
broader categories.

## References and publications

### Be comprehensive about publications

Add _all_ publications where you described, showcased, and benchmarked the
tool. Also add preprints.

There are several purposes for the precise publication tracking:
- the users may (and often will) sort the tools by the citation count, likely
  trying the most tested approach first
- the publications provide an additional form of documentation (often more
  colorful than the API docs)
- the links and citation counts provide a nice, easily available statistic
  about ELIXIR performance

### Use the Credit tab

The purpose of the "Credit" tab is not immediately obvious -- basically, you
can credit anything that has helped you with creating the tool. This is
currently used for at least 3 purposes:

- systematic tracking of non-ELIXIR collaborators (add ORCIDs)
- listing the **industry partners**
- listing affiliations to particular research institutions
  (**remember to add LCSB**)
