---
card_order: 600
layout: page
permalink: /external/publication/publish-repo/
shortcut: publication:publish-repo
redirect_from:
  - /cards/publication:publish-repo
  - /external/cards/publication:publish-repo
  - /publication/publish-repo
  - /external/external/publication/publish-repo/
---

# Publish a repository

By default, the repositories created on the LCSB Gitlab are private, i.e. they cannot be seen by anyone outside LCSB.

In order to change the visibility of the repository, you can do so by browsing to `Settings` and then expanding the section on `Visibility, project features, permissions`.

There, you can set the visibility to `Public`. It is important to note that the group must be public in order to change visibility of the repository.

Remember to follow the [LCSB policy regarding code development](https://howto.lcsb.uni.lu/?qms:LCSB-POL-BIC-07). In case of questions, please do not hesitate to contact the [R3 team](mailto:lcsb-r3@uni.lu).