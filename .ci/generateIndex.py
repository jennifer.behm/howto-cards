from generator import core, save

# walk through the folders with all the cards
ind, wl = core.core(["external"])

save.save_index(ind, "cards.md")
save.save_whitelist(wl, ".ci/whitelist.txt")
