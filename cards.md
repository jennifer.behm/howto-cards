---
layout: default
title: Cards
order: -1
---

{% include scripts.html %}

<style>
.noborderbox {
    border :0;
}
</style>

{% comment %}
	This code adds CSS that hides the index-boxes by default.
	It's in javascript, so that it doesn't effect the browsers with javascript disabled.
	How the correct boxes are shown then? box_hider.js shows them
{% endcomment %}
<script>
	var element = document.createElement('style');
	var content = document.createTextNode(".index-box {display: none;}");
	var head = document.getElementsByTagName('head');
	if (head.length > 0) {
		element.appendChild(content);
		head[0].appendChild(element);	
	}
</script>

<!-- index -->

<div class="index-box-container">

	<div class="index-box noborderbox" id="access-card">
		<h3>Access</h3>
		<ul>
			<li><a href="{{ 'external/access/harrenhal-access' | relative_url }}">HARRENHAL access</a></li>
			<li><a href="{{ 'external/access/lums-passwords' | relative_url }}">LUMS account</a></li>
			<li><a href="{{ 'external/access/passwords' | relative_url }}">Managing your passwords</a></li>
			<li><a href="{{ 'external/access/vpn-cerbere-access' | relative_url }}">VPN/CERBERE connection</a></li>
			<li><a href="{{ 'external/access/vpn-mobile' | relative_url }}">VPN connection on your mobile phone</a></li>
			<li><a href="{{ 'external/access/wifiNetworkAccessGuests' | relative_url }}">WiFi network access for guests</a></li>
		</ul>
	</div>
	<div class="index-box noborderbox" id="backup-card">
		<h3>Backup</h3>
		<ul>
			<li><a href="{{ 'external/backup/computer' | relative_url }}">Staff Computer</a></li>
		</ul>
	</div>
	<div class="index-box noborderbox" id="contribute-card">
		<h3>Contribute</h3>
		<ul>
			<li><a href="{{ 'external/contribute/git-clients' | relative_url }}">Git clients</a></li>
			<li><a href="{{ 'external/contribute/install-git' | relative_url }}">Installation of Git</a></li>
			<li><a href="{{ 'external/contribute/markdown' | relative_url }}">Markdown</a></li>
			<li><a href="{{ 'external/contribute/mirror-fork' | relative_url }}">Mirror fork automatically</a></li>
			<li><a href="{{ 'external/contribute/review' | relative_url }}">Reviewing in Git</a></li>
			<li><a href="{{ 'external/contribute/vscode' | relative_url }}">Contribute using Visual Studio Code</a></li>
			<li><a href="{{ 'external/contribute/web-ide' | relative_url }}">Contribute using Gitlab Web IDE</a></li>

		</ul>
	</div>
	<div class="index-box noborderbox" id="exchange-channels-card">
		<h3>Exchange channels</h3>
		<ul>
			<li><a href="{{ 'external/exchange-channels/lft' | relative_url }}">LCSB file transfer (LFT) Quick Guide</a></li>
			<li><a href="{{ 'external/exchange-channels/calendar' | relative_url }}">Sharing calendar in Microsoft Exchange</a></li>
			<li><a href="{{ 'external/exchange-channels/asperaweb' | relative_url }}">AsperaWEB Quick Guide</a></li>
			<li><a href="{{ 'external/exchange-channels/owncloud' | relative_url }}">Owncloud</a></li>
		</ul>
	</div>
	<div class="index-box noborderbox" id="general-card">
		<h3>General</h3>
		<ul>
			<li><a href="{{ 'external/general/usefulLinks' | relative_url }}">Useful links for living in Luxembourg</a></li>
			<li><a href="{{ 'external/general/getToLCSB' | relative_url }}">How to get to the Luxembourg Centre for Systems Biomedicine</a></li>
			<li><a href="{{ 'external/general/BelvalCampusMap' | relative_url }}">Belval Campus Map</a></li>
			<li><a href="{{ 'external/general/glossary' | relative_url }}">Glossary</a></li>
			<li><a href="{{ 'external/general/links' | relative_url }}">Important websites</a></li>
			<li><a href="{{ 'external/general/remote-working' | relative_url }}">Work remotely</a></li>
			<li><a href="{{ 'external/general/attend-webex' | relative_url }}">Attend the LCSB Team Meeting online (webex)</a></li>
		</ul>
	</div>
	<div class="index-box noborderbox" id="integrity-card">
		<h3>Integrity</h3>
		<ul>
			<li><a href="{{ 'external/integrity/checksum' | relative_url }}">Ensuring Integrity of Data Files with Checksums</a></li>
			<li><a href="{{ 'external/integrity/encryption/cloud' | relative_url }}">Data upload to cloud</a></li>
			<li><a href="{{ 'external/integrity/encryption/disk' | relative_url }}">Encrypting the Startup Disk for Your Laptop/Desktop</a></li>
			<li><a href="{{ 'external/integrity/encryption/file' | relative_url }}">Encrypting Files and Folders</a></li>
			<li><a href="{{ 'external/integrity/naming' | relative_url }}">Naming files</a></li>
			<li><a href="{{ 'external/integrity/organization' | relative_url }}">Organization</a></li>
			<li><a href="{{ 'external/integrity/sanitisation' | relative_url }}">Sanitising Data Files</a></li>
			<li><a href="{{ 'external/integrity/spreadsheets' | relative_url }}">Working with spreadsheets</a></li>

		</ul>
	</div>
	<div class="index-box noborderbox" id="lab-equipment-card">
		<h3>Lab: Equipment</h3>
		<ul>
			<li><a href="{{ 'external/lab-equipment/cryostorage' | relative_url }}">Utilization of the cryostorage</a></li>
			<li><a href="{{ 'external/lab-equipment/dishwasher-utilization-and-maintenance' | relative_url }}">Dishwasher utilization and maintenance</a></li>
			<li><a href="{{ 'external/lab-equipment/hoods' | relative_url }}">Laminar Flow - Fume Hood - Biosafety Cabinet: what are the differences and when to use them?</a></li>
			<li><a href="{{ 'external/lab-equipment/lightcycler' | relative_url }}">How to leave virtual instrument mode on the LightCycler</a></li>
			<li><a href="{{ 'external/lab-equipment/maintenance-of-fridges' | relative_url }}">Maintenance of fridges and freezers</a></li>
			<li><a href="{{ 'external/lab-equipment/maintenance_of_cold_traps' | relative_url }}">Maintenance of cold traps  </a></li>
			<li><a href="{{ 'external/lab-equipment/utilization-of-autoclaves' | relative_url }}">Utilization of autoclaves</a></li>
			<li><a href="{{ 'external/lab-equipment/utilization-of-balances' | relative_url }}">Utilization of balances</a></li>
			<li><a href="{{ 'external/lab-equipment/utilization-of-bsc' | relative_url }}">Biosafety Cabinets: good practices</a></li>
			<li><a href="{{ 'external/lab-equipment/utilization-of-pH-meter' | relative_url }}">Utilization of pH meter</a></li>

		</ul>
	</div>
	<div class="index-box noborderbox" id="lab-hsa-card">
		<h3>Lab: Health & Safety, Access</h3>
		<ul>
			<li><a href="{{ 'external/lab-hsa/handwashing' | relative_url }}">Handwashing</a></li>
			<li><a href="{{ 'external/lab-hsa/lab-coats' | relative_url }}">Lab coats</a></li>
			<li><a href="{{ 'external/lab-hsa/personal-alert-safety-system' | relative_url }}">Personal alert safety system (PASS)</a></li>
			<li><a href="{{ 'external/lab-hsa/shipment' | relative_url }}">Shipment of a biological or chemical sample</a></li>
			<li><a href="{{ 'external/lab-hsa/spill-bsc' | relative_url }}">How to deal with a spill in a BSC</a></li>
			<li><a href="{{ 'external/lab-hsa/waste' | relative_url }}">Chemical and Biological Waste Management</a></li>

		</ul>
	</div>
	<div class="index-box noborderbox" id="lab-quarks-card">
		<h3>Lab: Quarks</h3>
		<ul>
			<li><a href="{{ 'external/lab-quarks/book-lab-equipment' | relative_url }}">How to book a Lab Equipment in Quarks</a></li>
			<li><a href="{{ 'external/lab-quarks/quarks-general' | relative_url }}">Quarks - General information</a></li>

		</ul>
	</div>
	<div class="index-box noborderbox" id="on-offboarding-card">
		<h3>On offboarding</h3>
		<ul>
			<li><a href="{{ 'external/on-offboarding/checklistBeforeArriving' | relative_url }}">Newcomer Checklist before Arriving in Luxembourg</a></li>
			<li><a href="{{ 'external/on-offboarding/checklistArrival' | relative_url }}">Checklist upon arrival in Luxembourg</a></li>
			<li><a href="{{ 'external/on-offboarding/onboarding' | relative_url }}">Onboarding new members at the LCSB</a></li>
		</ul>
	</div>
	<div class="index-box noborderbox" id="publication-card">
		<h3>Publication</h3>
		<ul>
			<li><a href="{{ 'external/publication/publish-repo' | relative_url }}">Publish a repository</a></li>
			<li><a href="{{ 'external/publication/add-gitignore' | relative_url }}">Add a .gitignore to your repository</a></li>
			<li><a href="{{ 'external/publication/orcid' | relative_url }}">Obtain an ORCID</a></li>
			<li><a href="{{ 'external/publication/publishInBiotools' | relative_url }}">Publishing a tool in *bio.tools*</a></li>
			<li><a href="{{ 'external/publication/phdThesisTemplate' | relative_url }}">LaTeX template for a doctoral thesis at University of Luxembourg</a></li>
			<li><a href="{{ 'external/publication/10WaysImproveEnglish' | relative_url }}">10 ways to improve your English</a></li>
		</ul>
	</div>
</div><br><center><a href="{{ '/' | relative_url }}">go back</a></center><br><center><a href="{{ '/cards' | relative_url }}">Overview of all HowTo cards</a></center>